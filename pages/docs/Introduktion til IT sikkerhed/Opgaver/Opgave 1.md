---
hide:
  - footer
---

# Opgave 1 - Fagets læringsmål

## Information

I denne opgave skal i undersøge fagets læringsmål.

Formålet er at sikre at i ved:

- hvor læringsmålene kan findes
- hvad de betyder
- hvad i kan bruge dem til

og ikke mindst, bliver det lettere at huske læringsmålene hvis i har arbejdet lidt med dem.

## Instruktioner

1. Find læringsmålene i studieordningen på [https://www.ucl.dk/studiedokumenter/it-sikkerhed](https://www.ucl.dk/studiedokumenter/it-sikkerhed) og del linket mellem jer
2. I jeres team diskuter hvordan i forestiller jer at læringsmålene er relevante for faget og hvad i kan bruge dem til, begrund jeres valg og noter dem så vi kan diskutere det på klassen.

## Links

- [https://www.ucl.dk/studiedokumenter/it-sikkerhed](https://www.ucl.dk/studiedokumenter/it-sikkerhed)

---

## Besvarelse

1.  - [Studieordning - National del 2022](https://esdhweb.ucl.dk/D22-1980440.pdf)

    - [Studieordning - Institutionsdel 2022](https://esdhweb.ucl.dk/D22-1972441.pdf?nocache=79602e97-e7c5-47c1-a750-8e79e580a841)

2.
