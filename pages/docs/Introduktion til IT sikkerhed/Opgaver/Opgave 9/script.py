import sqlite3
from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths

files_path = Path(str(Path.cwd()) + '/database-programs/databases/')
print(files_path)

# Create and connect to database
conn = sqlite3.connect('music.db')

cur = conn.cursor()

# Create table in database
with conn:
    cur.execute('DROP TABLE IF EXISTS Tracks')
    cur.execute('CREATE TABLE Tracks (title TEXT, plays INTEGER)')

# Insert rows in tracks table
with conn:
    cur.execute('INSERT INTO Tracks (title, plays) VALUES (?, ?)', ('Thunderstruck', 20))
    cur.execute('INSERT INTO Tracks (title, plays) VALUES (?, ?)', ('My Way', 15))

# Update a row in tracks table
with conn: 
    cur.execute('UPDATE Tracks SET title = "Thunderrock", plays = 40 WHERE title = "Thunderstruck"')
    
# info to user
print('All rows in the Tracks table:')

# select values from table
cur.execute('SELECT title, plays FROM Tracks')

# print out the values
for row in cur:
    print(row)
    
# Create new table in database
with conn:
    cur.execute('DROP TABLE IF EXISTS New')
    cur.execute('CREATE TABLE New (name TEXT, sleeps BOOLEAN)')

# Insert 10 rows in new table
with conn:
    cur.execute('INSERT INTO New (name, sleeps) VALUES (?, ?)', ('Jens', 1))
    cur.execute('INSERT INTO New (name, sleeps) VALUES (?, ?)', ('Hans', 0))
    cur.execute('INSERT INTO New (name, sleeps) VALUES (?, ?)', ('Frans', 1))
    cur.execute('INSERT INTO New (name, sleeps) VALUES (?, ?)', ('Mads', 1))
    cur.execute('INSERT INTO New (name, sleeps) VALUES (?, ?)', ('Julie', 1))
    cur.execute('INSERT INTO New (name, sleeps) VALUES (?, ?)', ('Erik', 1))
    cur.execute('INSERT INTO New (name, sleeps) VALUES (?, ?)', ('A', 1))
    cur.execute('INSERT INTO New (name, sleeps) VALUES (?, ?)', ('B', 1))
    cur.execute('INSERT INTO New (name, sleeps) VALUES (?, ?)', ('C', 1))
    cur.execute('INSERT INTO New (name, sleeps) VALUES (?, ?)', ('D', 1))
    
# Delete a row in new table
with conn:
    cur.execute('DELETE FROM New WHERE name="Jens"')

# info to user
print('All rows in the New table:')

# select values from table
cur.execute('SELECT name, sleeps FROM New ORDER BY name ASC')

# print out the values
for row in cur:
    print(row)
    
cur.execute('DELETE FROM Tracks WHERE plays < 100')

# Close database connection
conn.close()