# Opgave 28 - CIA modellen

## Information

CIA modellen er central i arbejdet med informationssikkerhed.
Hvor modellen stammer fra er der uenighed om, men modellen er simpel og kan bruges mange steder i arbejdet med sikkerhed.

På dansk er modellen oversat til FIT, men det er mest udbredt at anvende den engelske betegnelse.

C står for Confidentiality (Fortrolighed) I står for Itegrity (Itegritet) A står for Availability (Tilgængelighed)

Formålet med denne øvelse er at blive bekendt med CIA modellen.
Instruktioner

Opgaven skal laves som gruppe/team

    Læs om CIA modellen i bogen "IT-Sikkerhed i praksis, en introduktion" kapitel 2.
    Vælg et af følgende scenarier som i vil vurdere i forhold til CIA:
        En password manager (software)
        En lægeklinik (virksomhed)
        Dine data på computere og cloud (...)
        Energileverandør (kritisk infrastruktur)
    Vurder, prioitér scenariet i forhold til CIA modellen. Noter og begrund jeres valg og overvejelser.
    Hvilke(n) type hacker angreb vil være mest aktuelt at tage forholdsregler imod? (DDos, ransomware, etc.)
    Hvilke forholdsregler kan i tage for at opretholde CIA i jeres scenarie (kryptering, adgangskontrol, hashing, logging etc.)
    Hver gruppe fremlægger det de er kommet frem til og alle giver feedback.

Links

    CIA model https://medium.com/learn-and-grow/what-is-the-cia-model-ae160bac21f
    CIA & DAD model https://medium.com/@yadavtejas249/cia-and-dad-triad-ef84a94f9aee

# Energileverandør (kritisk infrastruktur)

## Opgave 3

### Confidentiality

Sidste prioitet, da levrencen ikke bliver berørt.

###Integrity

Anden prioitet, da ændring af data, kan medføre til uforudsete.

### Availability

Højste prioitet fordi det er en kritisk i forhold til menskers liv og velvære.

## Opgave 4

Fishing

## Opgave 5

Spam filter
Idiot test
